import { Component } from "react";
import "./App.css";
import Card from "./components/Card/Card";
import "./style.css";

class App extends Component {
  render() {
    return (
      <body>
        <div className="container">
          <Card
            person={{
              name: "Itallo Dornelas",
              age: "24",
              occupation: "Desenvolvedor Web",
            }}
          />
        </div>
      </body>
    );
  }
}

export default App;
